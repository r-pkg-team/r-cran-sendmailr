r-cran-sendmailr (1.4-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2023 09:41:17 +0100

r-cran-sendmailr (1.3-1-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 05 Dec 2022 15:13:39 +0100

r-cran-sendmailr (1.2-1.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jun 2022 11:20:39 +0200

r-cran-sendmailr (1.2-1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Refer to specific version of license GPL-2.

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 May 2020 22:33:42 +0200

r-cran-sendmailr (1.2-1-4) unstable; urgency=medium

  * Rebuild to make sure r-cran-base64enc is included into Depends
    Closes: #900883, #900888

 -- Andreas Tille <tille@debian.org>  Wed, 06 Jun 2018 14:03:50 +0200

r-cran-sendmailr (1.2-1-3) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Spelling in d/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 01 Jun 2018 14:14:19 +0200

r-cran-sendmailr (1.2-1-2) unstable; urgency=medium

  * Fix Vcs-Browser
  * cme fix dpkg-control
  * debhelper 10
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * Convert from cdbs to dh-r
  * Secure URI in watch file
  * Canonical homepage for CRAN packages
  * Testsuite: autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Fri, 20 Oct 2017 14:36:42 +0200

r-cran-sendmailr (1.2-1-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * debian/copyright: Fix DEP5 license name

 -- Andreas Tille <tille@debian.org>  Tue, 30 Sep 2014 17:09:44 +0200

r-cran-sendmailr (1.1-2-1) unstable; urgency=low

  * Initial release (Closes: #751734)

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jun 2014 22:52:06 +0200
